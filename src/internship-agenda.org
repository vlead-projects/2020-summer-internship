#+TITLE: 2020 Summer Internship Agenda
#+AUTHOR: VLEAD
#+DATE: [2020-04-30 Thu]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document captures the internship agenda for 2020
  Summer Internship at VLEAD, IIIT Hyderabad.

* Internship Agenda

* Internship Program Structure
  2020 Summer Internship Program will have the following
  structure :
   
  + Each intern will be assigned with 2-3 services and
    will be working independently.

  + Each of them will be either building completely new
    experiments from scratch or re-factoring the already
    built experiments or may do both. Work for each intern
    is assigned in [[./project-assignments.org][Project Assignments]].
            
     


   
