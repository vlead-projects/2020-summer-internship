#+TITLE: 2020 Summer Internship at VLEAD, IIIT Hyderabad
#+AUTHOR: VLEAD
#+DATE: [2020-04-24 Fri]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  2020 Summer Internship program is envisaged to realize
  Virtual Labs related features.

* [[./internship-agenda.org][Internship Agenda]]
* [[./project-assignments.org][Project Assignments]]
* [[./on-boarding.org][On Boarding]]
* [[./week-wise-internship-plan.org][Week Wise Internship Plan]]
* [[./work-logs/index.org][Work Logs]]
* [[./minutes-of-meetings/index.org][Minutes of Meeting]]
* [[./internship-report.org][Internship Report]]
* [[./mentor-responsibilities.org][Mentor Responsibilites]]
