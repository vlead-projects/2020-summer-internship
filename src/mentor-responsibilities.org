#+TITLE: Mentor Responsibilities
#+AUTHOR: VLEAD
#+DATE: [2020-04-30 Thu]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  Responsibilities to be carried out by the mentors for the
  2020 Summer Internship at VLEAD, IIITH are captured here.

* Mentors
  Mentors for the Summer Internship 2020. 
    + *Primary Mentor*    : ==
    + *Secondary Mentor*  : ==, ==
    + *Approver*          : ==

  Please check [[./project-assignments.org][Project Assignments]], to know, to which intern
  you are assigned for. Also, refer review process in
  [[./on-boarding.org][on-boarding]].

* Responsibilities
  Responsibilities to be followed by each mentor:
*** Primary Mentor
    Expectations from the Primary Mentor:
    1. Daily mentoring and interactions with the interns
       should be made, to guide and check the status of
       their work.  This way, the decisions made and
       alternatives discussed are not lost.
   
    2. [[https://www.mountaingoatsoftware.com/agile/scrum/meetings/daily-scrum][Stand Ups]] :: Hold a stand up meeting the first thing
       in the morning of every day.  The meeting is meant
       to orally state what has been accomplished the
       previous day; what will be done that day; and what
       are the impediments faced in accomplishing the
       tasks.  It is the responsibility of the mentor to
       ensure these impediments are resolved on a priority
       basis by seeking clarification from the secondary
       mentors if needed.
      
    3. Ensure the discussions of the meetings are captured
       by the interns in the readme repository of their
       experiment group.

    4. Ensure that, the interns record the work logs daily
       [[./work-logs][here]]. Please check their work logs every Friday.

    5. Ensure that, a weekly milestone is created with a set
       of issues in their respective experiment group before
       the development process, to track the status of their
       work. 
      
       Milestone Instructions:
         - Proper description to each issue
         - Deadline for each for each issue and for the entire milestone 
         - Appropriate labels to each issue
         - Check whether the closing issue has a description
           with a commit id
       
       Please refer [[./on-boarding.org#milestone][Milestone]] section in [[./on-boarding.org][on-boarding]]
       document to know about it in detail.
  
    6. Make sure that interns follow [[https://gitversion.readthedocs.io/en/latest/git-branching-strategies/][Git Branching
       Strategies]] in respective experiment
       repositories. Interns are not supposed to work on
       "master" or "develop" branches. Initially they should
       create master and develop branches without any code
       base. Then they need to create a feature branch and
       then start working on it.

    7. Ensure that interns create a merge request to merge
       the code from "feature" branch to "develop" branch
       after the completion of each milestone. Review should
       be done only if they send a merge request.

    8. Do a overall review of work done once in a week
       before Friday. Add comments on merge request source
       code. Repeat this cycle until they fix the issue
       correctly. Please create a reviews directory in
       readme repository of their experiment group and add
       your comments. Please refer bubble sort experiment
       [[https://gitlab.com/vlead-projects/experiments/ds/bubble-sort/readme/tree/master/reviews][review]].

    9. Every release is a demonstrable set of features.  The
       planning is done in such a way that every Friday, a
       release is made.  Each release is planned as a
       milestone with a set of issues. Each issue captures a
       task. Ensure that the milestone implementation,
       review, fixes, release done before Friday. So, that
       it can be demonstrable on Friday.

    10. *Repository is the core asset* :: Every project that
         a mentor is entrusted with is realized within a
         repository.  All assets - diagrams, code,
         documentation , etc are part of this repository.
         If a diagram is made on the cloud, the link to that
         diagram in incorporated in the sources.

    11. *Literate Style* :: Projects are realized in
         literate style with proper description.  This is
         not an after thought.  Even exploration is done in
         literate style.

    12. Ensure that, they follow the [[https://gitlab.com/vlead-projects/experiments/on-boarding/blob/master/src/coding-standards.org][coding standards]]
   
    13. Make sure that the code and the experiment is well
        tested (Unit, Functionality and Performance
        Testing).

    14. *Slack Channel* : Please make sure that interns are
        available on the =2019-summer-interns=.

*** Secondary Mentor
    Expectations from the Secondary Mentor:
    1. Ensure that primary mentor is following all the above
       mentioned instructions.

    2. Hold a daily standup with the primary mentor to know
       the status of interns.
   
    3. After the final review of the experiment by the
       primary mentor, next round of review should be done
       by the secondary mentor. Then, this has to be passed
       on to the approver to give his/her
       comments. Experiment is allowed to release and get
       hosted on production, only after getting approval
       from the approver.

    4. Help the primary mentor resolve any issues faced
       during the course of the internship.

*** Approver
    Expectations from the Approver:
    1. Final review of the experiment structure/pedagogy,
       content and artefacts.

    2. Final review of the design specs of the experiment
       content and artefacts.

    3. Final review and approval to host the experiment on
       production.
       
