#+TITLE: 2020 Summer Internship Boarding Document
#+AUTHOR: VLEAD
#+DATE: [2020-04-30 Thu]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Welcome
  Welcome to Virtual Labs - 2020 Summer Internship Program @
  IIIT Hyderabad !!!

  We are delighted that you are joining us as a new intern. This
  program has been developed for students to gain practical
  training and hands-on, real world experience that will
  help them meet their educational goals and prepare them
  for a professional career.  The VLEAD team is here to
  support your transition. You can call on any of us to
  assist you.

* Instructions
** General Instructions 
*** Mentors 
    :PROPERTIES:
    :CUSTOM_ID: mentors
    :END:

    Mentors for the Summer Internship 2020. 
      + *Mentor-1* : =Raj=
      + *Mentor-2* : =Ojas Mohril=
      + *Mentor-3* : =Mrityunjay=

    Please check [[./project-assignments.org][Project Assignments]], in which each intern
    is assigned with particular mentors. Approach  mentor 
    for any kind of guidance in assigned project.
    
*** Intern Working Hours
    You will be need to be available on *Monday* to
    *Saturday* between *9:00 AM* to *5:00 PM*.


*** VLEAD Employee Working Hours
    VLEAD employees will be available on *Monday* to
    *Friday* between *9:00 AM* to *6:00 PM*.

*** Leave Policy
    You are not allowed to take any leave during your
    published days.
    
*** Work Logs
    1. Work logs should be written daily and pushed [[./work-logs][here]] by
       the end of the day (Mandatory). Please find the
       [[./work-logs/instructions.org][instructions]] to create work logs.

    2. You have to get your work logs approved by your
       [[#mentors][mentor]] on a weekly basis. You will also need
       to submit your approved work logs to
       *ravi.kiran@vlabs.ac.in* every month. 
      
*** Minutes of Meeting
    Discussions of the meetings should be captured in the
    "readme" repository of your experiment group. Please
    refer [[https://gitlab.com/vlead-projects/experiments/ds/bubble-sort/readme/tree/master/minutes-of-meetings][minutes of meetings]] in the [[https://gitlab.com/vlead-projects/experiments/ds/bubble-sort/readme/tree/master/minutes-of-meetings][readme]] repository of
    the [[https://gitlab.com/vlead-projects/experiments/ds/bubble-sort][Bubble Sort]] experiment to create minutes of
    meetings. Also, update the minutes of meetings link
    [[./minutes-of-meetings/index.org][here]].

*** Presentations
    1) Presentation will be once a week on jitsi through slack channel - every Friday

    2) A final presentation (with links to relevant
       documents) will need to be made during the last week
       of the internship against which the internship
       certificate will be issued.

*** Slack Channel
    Communication is made through [[https://vleadworkspace.slack.com][Slack]] during the
    internship period. If you need any guidance regarding
    anything, just post it on [[https://vleadworkspace.slack.com/archives/C012G7UKTC3][summer-interns-2020]]
    channel. To get access to this channel send out a mail
    to *ravi.kiran@vlabs.ac.in*.  Please be available at all
    times on [[https://vleadworkspace.slack.com/archives/C012G7UKTC3][summer-interns-2020]] channel on [[https://vleadworkspace.slack.com/messages][Slack]].
    
*** Guidance/Support
    Please reach out to any of the VLEAD employees or
    mentors for any kind of support.
  
*** Work Reporting
    Each intern should report their status of work to their
    mentor daily in the morning standup meetings,
    which will be held for 5-10 mins.

*** Internship Evaluation
    The interns will be evaluated on: 
    1. Attitute and Proactiveness
    2. Quality Delivery of the assigned tasks      
    3. Commitment towards the assigned task

*** Certificate
    You will be awarded a certificate only on the successful
    completion of your assigned work.

** Development Instructions 
*** Prerequisites
    :PROPERTIES:
    :CUSTOM_ID: prerequisites
    :END:

    1. Set up your machine with ubuntu OS
    2. Install and configure emacs on your machine
    3. Get comfortable with [[http://orgmode.org/][emacs]] editor and [[http://orgmode.org/][org-mode]]
    4. Get comfortable with front end technologies(HTML,
       CSS, JavaScript and any Front End or JS Frameworks)
    5. Get comfortable with [[https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control][version controlling]] using [[https://en.wikipedia.org/wiki/GitLab][Gitlab]]
         
*** Basic Development Standards
    :PROPERTIES:
    :CUSTOM_ID: basic-development-standards
    :END:

    These are the basic standards which needs to be followed
    during an experiment development.
    + [[#git-branching-strategies][Git branching strategies]] 
    + Creation of [[#milestone][milestone]] 
    
**** Git Branching Strategies
    :PROPERTIES:
    :CUSTOM_ID: git-branching-strategies
    :END:

    Every project repository should have "master" and
    "develop" branches which holds the production and
    staging code. To implement a [[#milestone][milestone]] create a *feature
    branch* from "develop" branch and then work on it.

    Please refer to [[https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging][Git Branching]] to know more about it.

**** Milestone
    :PROPERTIES:
    :CUSTOM_ID: milestone
    :END:
    
    Always create a milestone before the development process
    as, we are always working on a [[https://docs.gitlab.com/ee/user/project/milestones/][Git Milestone]] with a set
    of issues. So therefore, please create an *issue(s)*
    with an appropriate [[#issue-labeling][labels]], time duration and start
    working to realize them. On completion of an issue,
    update the issue with the *commit id* and an appropriate
    labels that refers to the work.  The issue itself should
    not contain the details, the document contains the
    details which is version controlled.
  
    *Note*: Please create one milestone per week. Every
    milestone should consist start and end date to track the
    status of your work.  You are not supposed to close any
    milestone or an issue, it's the [[#mentors][mentors]] responsibility.
    
**** Issue Labeling
    :PROPERTIES:
    :CUSTOM_ID: issue-labeling
    :END:

    Labelling an issue is mandatory, as it let's us know the
    status or progress of the issues.

    *What label to be assigned for an issue??*
    1. On creation of an issue, add label *Assigned*
    2. On start of an issue, remove label *Assigned*, add
       label *In Progress* with an other appropriate label
       which defines the issue
    3. On completion of an issue remove label *In Progress*
       and add label *Review*
    4. On completion of a review process it's the [[#mentors][mentors]]
       responsibility to remove label *Review* and add label
       *Closed*
       
*** Review Process
    :PROPERTIES:
    :CUSTOM_ID: review-process
    :END:

    Review of the [[#milestone][milestone]] implementation will be done by
    your [[#mentors][mentors]].  You need to go through the various stages
    of review to get the assigned project hosted.
    
    Please check [[./project-assignments.org][Project Assignments]], in which each intern
    is assigned with particular mentors.

    *Steps to be followed for the review :*
    1. Request for review by the assigned project developer
       through a [[https://docs.gitlab.com/ee/user/project/merge_requests/][merge request]]
       
    2. Get the project hosted on testing environment

    3. Get the code and overall assigned project reviewed by
        mentor with appropriate comments; If any
       issues fix it; Again get the project hosted on
       testing environment; Again get it reviewed; Repeat
       this until the issues are fixed.
 

       
*** Release
    :PROPERTIES:
    :CUSTOM_ID: release
    :END:

    [[https://git-scm.com/book/en/v2/Git-Basics-Tagging][Tag]] the project repo build with a specific version and release
    it.

     

 
    
    
