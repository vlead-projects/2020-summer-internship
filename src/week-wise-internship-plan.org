#+TITLE: Week Wise Internship Plan
#+AUTHOR:VLEAD
#+DATE: [2020-04-30 Thu]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document captures the week wise internship plan for
  the 2020 Summer Interns at VLEAD, IIITH.

* Plan for Interns Working on Experiment Development from Scratch
** Time Estimation 
   One Experiment :: *1 Month*

** Work Flow
   Work Flow of the Experiment Development should be as,
   defined below:
     1. Define the structure for the experiment 
     2. Define the design specs for the experiment
     3. Authoring content and creation of images
     4. Design artefacts
     5. Video recording
     6. Peer review 

** Weekly Plan
   Please find the weekly plan below:
**** Week 1:
     Tasks to be accomplished in the first week:
       1. Exp structure and Design specs 
       2. Content and images in place
       3. Design of the first artefact
       4. Review, Fixes and Hosting (Cyclic Process)
       5. Presentation 
       6. Peer review of other experiments 

**** Week 2:
     Tasks to be accomplished in the second week:
       1. Peer review fixes
       2. Design and Implementation of the subsequent
          artifacts
       3. Review, Fixes and Hosting (Cyclic Process)
       4. Presentation 
       5. Peer review of other experiments    
          
**** Week 3:
     Tasks to be accomplished in the third week:
       1. Peer review fixes
       2. Design and Implementation of the subsequent
          artifacts
       3. Review, Fixes and Hosting (Cyclic Process)
       4. Presentation 
       5. Peer review of other experiments   

**** Week 4:
     Tasks to be accomplished in the fourth week: 
       1. Peer review fixes
       2. Design and Implementation of the subsequent
          artefacts
       3. Video Recording
       4. Review, Fixes and Hosting (Cyclic Process)
       5. Presentation
  
*Note:* Video recording should be done only after the
completion of the artefacts.

* Plan for Interns Working on Built Experiments (Fixes)
** Time Estimation 
   One Experiment :: *2 Weeks*

** Weekly Plan
   Please find the weekly plan below:
**** Week 1:
     Tasks to be accomplished in the first week:
       1. Test the experiment: 
          + Check if the experiment group repositories are
            properly structured and check if they have all
            the essential documentations
          + Check if the images are matching the vlead
            colour theme
          + Check the responsiveness and the look and feel
            of the image
          + Test the functionality of the artefacts
          + Test whether the artefacts are responsive  
          + Do spell check for the content
          + Check if the artefacts are built as per coding
            standards 
        
          If any issues, found please log the issues as a
          milestone and work on it.
   
       2. Implement/Fix the logged issues
       3. Review, Fixes and Hosting (Cyclic Process) 
       4. Presentation
       5. Peer review of other experiments

**** Week 2:
     Tasks to be accomplished in the second week:
       1. Peer review fixes 
       2. Implement/Fix the logged issues
       3. Review, Fixes and Hosting (Cyclic Process) 
       4. Presentation of the overall experiment
       5. Video recording, review and fixes in the video and
          experiment hosting       
       6. Peer review of other experiments

**** Week 3:
     Same as [[#fixes-week1][week 1]] for an other experiment

**** Week 4:
     Same as [[#fixes-week2][week 2]] for an other experiment
     
